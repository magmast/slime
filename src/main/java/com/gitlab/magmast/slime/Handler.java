package com.gitlab.magmast.slime;

import com.gitlab.magmast.slime.http.Request;
import com.gitlab.magmast.slime.http.Response;

public interface Handler {
    /**
     * A handler which does nothing despite returning an empty response.
     */
    Handler noop = (request) -> Response.empty();

    Response handle(Request request);
}
