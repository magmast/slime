package com.gitlab.magmast.slime.http;

public record Request(Method method, String path) {
}
