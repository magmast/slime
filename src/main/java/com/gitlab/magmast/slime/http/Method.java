package com.gitlab.magmast.slime.http;

public enum Method {
    GET,
    POST,
    PATCH,
    PUT,
    DELETE,
    OPTIONS,
    HEAD;

    public static Method fromString(String str) {
        return switch (str) {
            case "GET" -> Method.GET;
            case "POST" -> Method.POST;
            case "PATCH" -> Method.PATCH;
            case "PUT" -> Method.PUT;
            case "DELETE" -> Method.DELETE;
            case "OPTIONS" -> Method.OPTIONS;
            case "HEAD" -> Method.HEAD;
            default -> throw new IllegalArgumentException();
        };
    }
}
