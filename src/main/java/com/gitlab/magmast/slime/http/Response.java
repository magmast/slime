package com.gitlab.magmast.slime.http;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public record Response(int statusCode, Map<String, String> headers, byte[] body) {
    public static Response empty() {
        return empty(200);
    }

    public static Response empty(int statusCode) {
        return new Response(statusCode, Map.of(), new byte[]{});
    }

    public static Response text(String body) {
        return new Response(
                200,
                Map.of("Content-Type", "text/plain"),
                body.getBytes(StandardCharsets.UTF_8)
        );
    }
}
