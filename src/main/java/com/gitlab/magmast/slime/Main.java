package com.gitlab.magmast.slime;

import com.gitlab.magmast.slime.http.Response;
import com.gitlab.magmast.slime.router.Router;
import com.sun.net.httpserver.HttpServer;

import java.net.InetSocketAddress;

public class Main {
    public static void main(String[] args) throws Exception {
        final var router = new Router();
        router.get("/", (request) -> Response.text("Index"));
        router.get("/hello", (request) -> Response.text("Hello, World!"));
        router.get(
                "/logger",
                (request, next) -> {
                    System.out.println("Hello, World!");
                    return next.handle(request);
                }
        );

        final var app = new Application();
        app.use(router);

        final var server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/", app.getHttpHandler());
        server.start();
    }
}
