package com.gitlab.magmast.slime.router;

import com.gitlab.magmast.slime.Handler;
import com.gitlab.magmast.slime.http.Method;
import com.gitlab.magmast.slime.http.Request;
import com.gitlab.magmast.slime.http.Response;
import com.gitlab.magmast.slime.middleware.ComposeMiddleware;
import com.gitlab.magmast.slime.middleware.Middleware;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Router implements Middleware {
    final ArrayList<Route> routes = new ArrayList<>();

    public Route get(String path, Handler handler) {
        return get(path, (request, next) -> handler.handle(request));
    }

    public Route get(String path, Middleware... middlewares) {
        final var route = new Route(Set.of(Method.GET), path, List.of(middlewares));
        routes.add(route);
        return route;
    }

    @Override
    public Response handle(Request request, Handler next) {
        final var route = routes.stream()
                .filter(r -> r.methods().contains(request.method()))
                .filter(r -> r.path().equals(request.path()))
                .findFirst();

        if (route.isEmpty()) {
            return Response.empty(404);
        }

        return ComposeMiddleware.execute(route.get().middlewares(), request, next);
    }
}
