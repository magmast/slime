package com.gitlab.magmast.slime.router;

import com.gitlab.magmast.slime.http.Method;
import com.gitlab.magmast.slime.middleware.Middleware;

import java.util.List;
import java.util.Set;

public record Route(Set<Method> methods, String path, List<Middleware> middlewares) {
    public Route {
        assert (!methods.isEmpty());
        assert (!middlewares.isEmpty());
    }
}
