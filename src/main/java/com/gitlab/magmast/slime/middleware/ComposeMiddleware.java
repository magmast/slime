package com.gitlab.magmast.slime.middleware;

import com.gitlab.magmast.slime.Handler;
import com.gitlab.magmast.slime.http.Request;
import com.gitlab.magmast.slime.http.Response;
import com.gitlab.magmast.slime.utils.IterableUtils;

import java.util.List;

/**
 * Composes multiple middlewares into a single one.
 *
 * @param middlewares Composed middlewares.
 */
public record ComposeMiddleware(List<Middleware> middlewares) implements Middleware {
    /**
     * Composes middlewares and executes them.
     *
     * @return Response returned from the composed middleware.
     */
    public static Response execute(List<Middleware> middlewares, Request request) {
        return execute(middlewares, request, Handler.noop);
    }

    /**
     * Composes middlewares and executes them.
     *
     * @return Response returned from the composed middleware.
     */
    public static Response execute(List<Middleware> middlewares, Request request, Handler next) {
        final var middleware = new ComposeMiddleware(middlewares);
        return middleware.handle(request, Handler.noop);
    }

    @Override
    public Response handle(Request request, Handler next) {
        final var handler = IterableUtils.foldRight(
                middlewares,
                next,
                (acc, middleware) -> (req) -> middleware.handle(req, acc)
        );
        return handler.handle(request);
    }
}
