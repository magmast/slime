package com.gitlab.magmast.slime.middleware;

import com.gitlab.magmast.slime.Handler;
import com.gitlab.magmast.slime.http.Request;
import com.gitlab.magmast.slime.http.Response;

public interface Middleware {
    Response handle(Request request, Handler next);
}
