package com.gitlab.magmast.slime;

import com.gitlab.magmast.slime.http.Method;
import com.gitlab.magmast.slime.http.Request;
import com.gitlab.magmast.slime.middleware.ComposeMiddleware;
import com.gitlab.magmast.slime.middleware.Middleware;
import com.sun.net.httpserver.HttpHandler;

import java.util.ArrayList;

public class Application {
    private final ArrayList<Middleware> middlewares = new ArrayList<>();

    Application use(Middleware middleware) {
        middlewares.add(middleware);
        return this;
    }

    HttpHandler getHttpHandler() {
        return (exchange) -> {
            final var request = new Request(
                    Method.fromString(exchange.getRequestMethod()),
                    exchange.getRequestURI().getPath()
            );
            final var response = ComposeMiddleware.execute(middlewares, request);

            exchange.sendResponseHeaders(response.statusCode(), response.body().length);

            final var outputStream = exchange.getResponseBody();
            outputStream.write(response.body());
            outputStream.close();
        };
    }
}
