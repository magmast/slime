package com.gitlab.magmast.slime.utils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.function.BiFunction;

public class IterableUtils {
    public static <T, U> U fold(@NotNull Iterable<T> iterable, U initialValue, BiFunction<U, T, U> accumulator) {
        var value = initialValue;
        for (final var item : iterable) {
            value = accumulator.apply(value, item);
        }
        return value;
    }

    public static <T, U> U foldRight(@NotNull Iterable<T> iterable, U initialValue, BiFunction<U, T, U> accumulator) {
        final var list = toArrayList(iterable);
        Collections.reverse(list);
        return fold(list, initialValue, accumulator);
    }

    public static <T> ArrayList<T> toArrayList(Iterable<T> iterable) {
        if (iterable instanceof Collection<T> collection) {
            return new ArrayList<>(collection);
        }

        final var list = new ArrayList<T>();

        for (final var item : iterable) {
            list.add(item);
        }

        return list;
    }
}
